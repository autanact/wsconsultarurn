package co.net.une.www.svcEJB;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Servicios WEB GSS SmallWorld 4.3 - Gesti�n de Direcciones Excepcionadas
##	Archivo: WSConsultarURNBean.java
##	Contenido: Clase que contiene la implementaci�n del servicio WSConsultarURN, para obtener listas de URNs en SW
##	Autor: Freddy Molina
##  Fecha creaci�n: 13-12-2015
##	Fecha �ltima modificaci�n: 19-02-2016
##	Historial de cambios: 
##	19-02-2016	FJM		Primera versi�n
##
##**********************************************************************************************************************************
*/
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import co.net.une.www.gis.GisRespuestaGeneralType_GDE;
import co.net.une.www.gis.WSConsultarURNRS;
import co.net.une.www.gis.WSConsultarURNRSType;

import com.gesmallworld.gss.lib.service.ServiceLocal;
import com.gesmallworld.gss.lib.service.magik.MagikService;
import com.gesmallworld.gss.lib.auth.AuthorisationException;
import com.gesmallworld.gss.lib.locator.ServiceLocator;
import com.gesmallworld.gss.lib.locator.ServiceLocatorException;
import com.gesmallworld.gss.lib.log.SWLog;
import com.gesmallworld.gss.lib.request.BusinessResponse;
import com.gesmallworld.gss.lib.request.ParameterException;
import com.gesmallworld.gss.lib.request.Request;

import javax.ejb.SessionBean;

import org.apache.log4j.Level;

import com.gesmallworld.gss.lib.request.Response;
import com.gesmallworld.gss.lib.service.magik.MagikService.StateHandling;

/**
 * Service proxy class generated by GSS Code Generator.
 * 
 * @ejb.resource-ref res-type="javax.resource.cci.ConnectionFactory"
 *                   res-auth="Container" res-ref-name="eis/SmallworldServer"
 *                   jndi-name="eis/SmallworldServer"
 *                   res-sharing-scope="Shareable"
 * @ejb.interface local-extends=
 *                "javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ChainableServiceLocal"
 *                package="co.net.une.www.interfaces"
 * @ejb.bean view-type="local" name="WSConsultarURN" type="Stateless"
 *           local-jndi-name="ejb/WSConsultarURNLocal" transaction-type="Bean"
 * @ejb.home local-extends="javax.ejb.EJBLocalHome"
 *           package="co.net.une.www.interfaces"
 */
@StateHandling(serviceProvider = "wsconsultar_urn_service_provider")
public class WSConsultarURNBean extends MagikService implements SessionBean {

	/**
	 * Local jndi name in use for this service bean.
	 */
	public static final String LOCAL_JNDI_NAME = "ejb/WSConsultarURNLocal";

	/**
	 * Serialisation ID.
	 */
	private static final long serialVersionUID = 1L;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.traducciones-consURN");

	private static final String keyEmptyMessage = "validate.emptyField";
	
	private static final String keyEmptyMessageCode = "validate.emptyField.code";

	private static final String keyFunctionName = "validate.functionName";

	private static final String KeyParameterException = "validate.parameterException";

	private static final String KeyServiceLocatorException = "validate.serviceLocatorException";

	private static final String KeyAuthorisationException = "validate.authorisationException";

	private static final String KeyGeneralException = "validate.generalException";

	/**
	 * Constructor.
	 */
	public WSConsultarURNBean() {
		super();
	}

	/**
	 * Generated service proxy method. Corresponds to a service of a Magik
	 * Service Provider.
	 * 
	 * @ejb.interface-method
	 * @param request
	 *            A request object as specified in the service description file.
	 * @return A response instance
	 */
	@SuppressWarnings("unchecked")
	@EISMapping(value = "consultar_urn")
	public Response consultarURN(Request request) {

		// Definimos las variables de ambiente
		WSConsultarURNRSType respuestaGssServiceEJB = new WSConsultarURNRSType();
		GisRespuestaGeneralType_GDE respuestaGeneralGssServiceEJB = new GisRespuestaGeneralType_GDE();
		WSConsultarURNRS consultarURNExc = new WSConsultarURNRS();
		consultarURNExc.setWSConsultarURNRS(respuestaGssServiceEJB);
		respuestaGssServiceEJB.setGisRespuestaProceso(respuestaGeneralGssServiceEJB);

		// inicializamos la respuesta del servicio
		inicializarParametrosRespuesta(respuestaGssServiceEJB);

		Response result = new BusinessResponse(request);

		try {

			// obtenemos los par�metros del request
			String nombreTabla = "".equals((String) request.getParameter("nombreTabla")) ? null : (String) request.getParameter("nombreTabla");
			String nombreDataset = "".equals((String) request.getParameter("nombreDataset")) ? null : (String) request.getParameter("nombreDataset");
			String nombresCamposConsulta = "".equals((String) request.getParameter("nombresCamposConsulta")) ? null : (String) request.getParameter("nombresCamposConsulta");
			String valoresCamposConsulta = "".equals((String) request.getParameter("valoresCamposConsulta")) ? null : (String) request.getParameter("valoresCamposConsulta");

			// validamos lo par�metros obligatorios
			if ((nombreTabla == null) || (nombreDataset == null)) {

				// Faltan par�metros obligatorios
				notificarFaltanParametrosObligatorios(respuestaGeneralGssServiceEJB);
				
			} else {

				// Invocamos la llamada encadenada

				Request r = request.createChainedRequest("ejb/ConsultarURNLocal", "consultarURN");

				r.setParameter("nombreTabla", nombreTabla);
				r.setParameter("nombreDataset", nombreDataset);
				r.setParameter("nombresCamposConsulta", nombresCamposConsulta);
				r.setParameter("valoresCamposConsulta", valoresCamposConsulta);

				ServiceLocal s = (ServiceLocal) ServiceLocator.getInstance().getSLSB("ejb/ConsultarURNLocal");

				Response gssServiceResponse = s.makeRequest(r);

				Map<String, Object> mapConsultarDirExc = gssServiceResponse.getResponses();

				// Validamos la respuesta del servicio

				String codigoRespuesta = (String) mapConsultarDirExc.get("codigoRespuesta");
				if ("KO".equals(codigoRespuesta)) {
					
					// El servicio notific� una falla
					notificarFallaServicio(respuestaGeneralGssServiceEJB, mapConsultarDirExc);
				} else {
						
					List<String> resultados = (List <String>) mapConsultarDirExc.get("resultados");

					if (resultados!=null) {
						String[] urns = resultados.toArray(new String[0]);
	
						respuestaGssServiceEJB.setResultados(urns);
						
						if ((String) mapConsultarDirExc.get("totalResultados")!=null)
							respuestaGssServiceEJB.setTotalResultados((String) mapConsultarDirExc.get("totalResultados"));
					}
					// El servicio funcion� satisfactoriamente
					notificarExitoServicio(respuestaGeneralGssServiceEJB);
				}
			}

		} catch (ParameterException e) {
			notificarExceptionTipificada(respuestaGeneralGssServiceEJB, KeyParameterException, keyFunctionName);
			e.printStackTrace();
		} catch (ServiceLocatorException e) {
			notificarExceptionTipificada(respuestaGeneralGssServiceEJB, KeyServiceLocatorException, keyFunctionName);
			e.printStackTrace();
		} catch (AuthorisationException e) {
			notificarExceptionTipificada(respuestaGeneralGssServiceEJB, KeyAuthorisationException, keyFunctionName);
			e.printStackTrace();
		} catch (Exception e) {
			notificarExceptionGeneral(respuestaGeneralGssServiceEJB, KeyGeneralException, keyFunctionName, e);
			e.printStackTrace();
		} finally {
			try {
				// Anexamos la respuesta del servicio
				result.addResponse("response", respuestaGssServiceEJB);
			} catch (ParameterException e) {
				SWLog.log(Level.ERROR, this, e.getMessage());
			}
		}
		
		// Enviamos la respuesta
		return result;
	}

	/**
	 * Notificar que faltan par�metros obligatorios
	 * @param resp
	 */
	private void notificarFaltanParametrosObligatorios(GisRespuestaGeneralType_GDE resp){
		resp.setCodigoError(rb.getString(keyEmptyMessageCode));
		resp.setDescripcionError(rb.getString(keyEmptyMessage));
	}

	/**
	 * LLenar par�metros de respuesta satisfactoria
	 * @param resp
	 */
	private void notificarExitoServicio(GisRespuestaGeneralType_GDE resp) {
		resp.setCodigoRespuesta("OK");
	}
	
	/**
	 * Llenar par�metros de respuesta con fallo provenientes de servicio encadenado
	 * @param resp
	 * @param mapRespuesta
	 */
	private void notificarFallaServicio(GisRespuestaGeneralType_GDE resp, Map<String, Object> mapRespuesta) {
		resp.setCodigoError((String) mapRespuesta.get("codigoError"));
		resp.setDescripcionError((String) mapRespuesta.get("descripcionError"));
	}

	/**
	 * Inicializar par�metros de respuesta del servicio
	 * @param resp
	 */
	private void inicializarParametrosRespuesta(WSConsultarURNRSType resp) {
		String[] resultArray  = {"0"};
		resp.setResultados(resultArray);
		resp.setTotalResultados("0");
		resp.getGisRespuestaProceso().setCodigoRespuesta("KO");
		resp.getGisRespuestaProceso().setCodigoError("");
		resp.getGisRespuestaProceso().setDescripcionError("");
		
	}

	/**
	 * Llenar par�metros de respuesta con fallo tipificado a partir Excepciones tipificadas
	 * @param resp
	 * @param keyMessage
	 * @param value
	 */
	private void notificarExceptionTipificada(GisRespuestaGeneralType_GDE resp, String keyMessage, String value) {
		resp.setDescripcionError(MessageFormat.format(rb.getString(keyMessage), rb.getString(value)));
	}

	/**
	 * Llenar par�metros de respuesta con fallo tipificado a partir Excepcion general
	 * @param resp
	 * @param keyMessage
	 * @param value
	 * @param e
	 */
	private void notificarExceptionGeneral(GisRespuestaGeneralType_GDE resp, String keyMessage, String value, Exception e) {
		resp.setDescripcionError(MessageFormat.format(rb.getString(keyMessage), rb.getString(value)) + ": " + e.getMessage());
	}
}
